const express = require('express');
const router = express.Router();
const Room = require('../models/Room');

router.get('/', async (req, res) => {
    try {
        const rooms = await Room.find();
        res.json(rooms);
    } catch (e) {
        res.status(400).json({message: e.message});
    }
});

router.get('/:roomId', async (req, res) => {

    io.on('connection', (socket) => {
        console.log('connected!');
        socket.emit('hello');
    });

    try {
        const room = await Room.findById(req.params.roomId);
        res.json(room);
    } catch (e) {
        res.status(400).json({message: e.message});
    }
});

router.post('/', async (req, res) => {
    const room = new Room({
        name: req.body.name,
        description: req.body.description
    });

    try {
        const newRoom = await room.save();
        res.status(201).json(newRoom);
    } catch (e) {
        res.status(400).json({message: e.message});
    }
});

router.put('/:roomId', async (req, res) => {
    try {
        const updatedRoom = await Room.updateOne(
            {_id: req.params.roomId},
            {$set: {
                    name: req.body.name,
                    description: req.body.description
                }
            }
        );
        res.status(200).json(updatedRoom);
    } catch (e) {
        res.status(400).json({message: e.message});
    }
});

router.delete('/:roomId', async (req, res) => {
    try {
        const removed = await Room.remove({_id: req.params.roomId});
        res.status(204).json(null);
    } catch (e) {
        res.status(400).json({message: e.message});
    }
});

module.exports = router;