const express = require('express');
// const mongoose = require('mongoose');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const uuidv1 = require('uuid/v1');

const port = process.env.PORT || 5000;
require('dotenv/config');

const pagesRoute = require('./routes/pages');
const postsRoute = require('./routes/rooms');

// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());
// app.use('/', pagesRoute);
// app.use('/rooms', postsRoute);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/sessions/index.html');
});

// Connect to db
// mongoose.connect(
//     process.env.DB_CONNECTION,
//     {useNewUrlParser: true, useUnifiedTopology: true},
//     () => console.log('Connected to db!')
// );

server.listen(port);
console.log(`Server is listening on port ${port}...`);

let usersSocketsMap = [];
let sessions = [];

io.set('origins', '*:*');

const usersSockets = io.of('users')
    .on('connection', (socket) => {
        socket.on('user_created', (data) => {
            console.log('User created');
            socket.emit('set_user_data', {
                userId: uuidv1(),
                username: data.username
            });
        });
    });

const sessionSockets = io.of('sessions')
    .on('connection', (socket) => {
        sessionSockets.emit('refresh_sessions', sessions);

        // Map userId to socketId
        socket.emit('auth_required');
        socket.on('auth', (data) => {
            console.log('dataOnAuth', data);

            usersSocketsMap.push({
                socketId: socket.id,
                userId: data.userId
            });

            console.log('usersSocketsMap', usersSocketsMap);
        });

        // Session created
        socket.on('session_created', (data) => {
            console.log('sessionCreatedData', data);
            const sessionId = uuidv1();
            sessions.push({
                sessionId: sessionId,
                users: [
                    {
                        userId: data.userId,
                        username: data.username
                    }
                ],
                color: data.color,
                estimationType: data.estimationType
            });

            socket.join(sessionId);

            // Emit this to all clients on channel
            sessionSockets.emit('refresh_sessions', sessions);
        });

        // Get single session
        socket.on('session_joined', (data) => {
            console.log('sessionJoinedDAta', data);

            const session = sessions.find(session => session.sessionId === data.sessionId);
            const user = session.users.find(user => user.userId === data.userId);

            socket.join(data.sessionId);

            if (!user) {
                sessions = sessions.map((session) => {
                    if (session.sessionId !== data.sessionId) return session
                    return {
                        ...session,
                        users: [...session.users, {username: data.username, userId: data.userId}]
                    }
                });

                const session = sessions.find(session => session.sessionId === data.sessionId);

                sessionSockets.to(data.sessionId).emit('refresh_single_session', session);
                sessionSockets.emit('refresh_sessions', sessions);
            }
        });

        // User left session
        socket.on('session_left', (data) => {

            console.log('sessionLeftData', data);

            sessions = sessions.map((session) => ({
                ...session,
                users: session.users.filter((user) => user.userId !== data.userId)
            }));

            sessions = sessions.map((session) => {
                if (session.sessionId === data.sessionId) {
                    return ({
                        ...session,
                        users: session.users.map(user => ({
                            ...user,
                            vote: null
                        }))
                    });
                }

                return session;
            });

            sessions.forEach((session) => {
                console.log(`Session id: ${session.sessionId}`, session);
            });

            sessions = sessions.filter((session) => session.users.length);
            const session = sessions.find(session => session.sessionId === data.sessionId);

            if (session) {
                sessionSockets.to(data.sessionId).emit('refresh_single_session', session);
                sessionSockets.to(data.sessionId).emit('hide_results');
            }

            sessionSockets.emit('refresh_sessions', sessions);
        });

        // User voted
        socket.on('user_voted', (data) => {
            console.log('userVotedData', data);

            sessions = sessions.map((session) => ({
                ...session,
                users: session.users.map((user) => {
                    console.log('userVotedUser', user);
                    if (user.userId === data.userId) {
                         user.vote = data.vote;
                    }
                    return user
                })
            }));


            const session = sessions.find(session => session.sessionId === data.sessionId);

            // Check if everyone voted
            let everyoneVoted = true;

            session.users.forEach((user) => {
                if (typeof user.vote === 'undefined' || user.vote === null) {
                    everyoneVoted = false;
                }
            });

            if (everyoneVoted) {
                sessionSockets.to(data.sessionId).emit('show_results');
            }

            sessionSockets.to(data.sessionId).emit('refresh_single_session', session);
        });

        // Clear results
        socket.on('session_results_cleared', (data) => {

            console.log('sessionResultsClearedData', data);

            sessions = sessions.map((session) => {
                if (session.sessionId === data.sessionId) {
                    return ({
                       ...session,
                       users: session.users.map(user => ({
                           ...user,
                           vote: null
                       }))
                    });
                }

                return session;
            });

            const session = sessions.find(session => session.sessionId === data.sessionId);

            sessionSockets.to(data.sessionId).emit('hide_results');
            sessionSockets.to(data.sessionId).emit('refresh_single_session', session);
        });

        // User left/disconnected
        socket.on('disconnect', () => {
            console.log(`Socket id: ${socket.id} disconnected`);

            const userSocket = usersSocketsMap.find((userSocket) => userSocket.socketId === socket.id);

            // Remove user from session
            if (userSocket) {
                sessions = sessions.map((session) => ({
                    ...session,
                    users: session.users.filter(user => user.userId !== userSocket.userId)
                }));

                console.log(`User id: ${userSocket.userId} socket id: ${userSocket.socketId} disconnected`);
            }

            // Remove the mapping
            usersSocketsMap = usersSocketsMap.filter((userSocket) => userSocket.socketId !== socket.id);

            // Remove sessions with no users (everyone left)
            sessions = sessions.filter((session) => session.users.length);

            sessionSockets.emit('refresh_sessions', sessions);
        });
    });
